#include <crc_core.h>

#include <assert.h>
#include <iostream>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

namespace {

constexpr std::uint8_t check_str[] = { 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39 };
constexpr std::uint8_t check_str_part1[] = { 0x31, 0x32, 0x33, 0x34, 0x35 };
constexpr std::uint8_t check_str_part2[] = { 0x36, 0x37, 0x38, 0x39 };

constexpr crc::core::Algorithm<std::uint64_t> algorithm_at_compile_time = {
    .width = 40,
    .poly = UINT64_C(0x0004820009),
    .init = UINT64_C(0x0000000000),
    .refin = false,
    .refout = false,
    .xorout = UINT64_C(0xffffffffff),
    .check = UINT64_C(0xd4164fc646),
    .residue = UINT64_C(0xc4ff8071ff),
};
constexpr crc::core::Crc<std::uint64_t> crc_at_compile_time(algorithm_at_compile_time);
constexpr std::uint64_t checksum_at_compile_time =
    crc_at_compile_time.checksum(check_str, ARRAY_SIZE(check_str));

template<typename T>
void test_crc(
    std::vector<std::tuple<std::string, std::uint64_t, std::uint64_t>> &failures,
    const std::string &name,
    const crc::core::Algorithm<T> &algorithm)
{
    const crc::core::Crc<T> crc_to_test(algorithm);
    T calculated_crc = crc_to_test.init();
    crc_to_test.update(calculated_crc, check_str_part1, ARRAY_SIZE(check_str_part1));
    crc_to_test.update(calculated_crc, check_str_part2, ARRAY_SIZE(check_str_part2));
    crc_to_test.finalize(calculated_crc);

    if (calculated_crc != crc_to_test.algorithm.check) {
        failures.emplace_back(name, calculated_crc, crc_to_test.algorithm.check);
    }
}

} // namespace

int main(void)
{
    std::vector<std::tuple<std::string, std::uint64_t, std::uint64_t>> failures;

    const auto crc8_algorithms = {
        std::make_tuple(
            "CRC-3/GSM",
            crc::core::Algorithm<std::uint8_t>{
                .width = 3,
                .poly = UINT8_C(0x3),
                .init = UINT8_C(0x0),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x7),
                .check = UINT8_C(0x4),
                .residue = UINT8_C(0x2),
            }),
        std::make_tuple(
            "CRC-3/ROHC",
            crc::core::Algorithm<std::uint8_t>{
                .width = 3,
                .poly = UINT8_C(0x3),
                .init = UINT8_C(0x7),
                .refin = true,
                .refout = true,
                .xorout = UINT8_C(0x0),
                .check = UINT8_C(0x6),
                .residue = UINT8_C(0x0),
            }),

        std::make_tuple(
            "CRC-4/G-704",
            crc::core::Algorithm<std::uint8_t>{
                .width = 4,
                .poly = UINT8_C(0x3),
                .init = UINT8_C(0x0),
                .refin = true,
                .refout = true,
                .xorout = UINT8_C(0x0),
                .check = UINT8_C(0x7),
                .residue = UINT8_C(0x0),
            }),
        std::make_tuple(
            "CRC-4/INTERLAKEN",
            crc::core::Algorithm<std::uint8_t>{
                .width = 4,
                .poly = UINT8_C(0x3),
                .init = UINT8_C(0xf),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0xf),
                .check = UINT8_C(0xb),
                .residue = UINT8_C(0x2),
            }),

        std::make_tuple(
            "CRC-5/EPC-C1G2",
            crc::core::Algorithm<std::uint8_t>{
                .width = 5,
                .poly = UINT8_C(0x09),
                .init = UINT8_C(0x09),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x00),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-5/G-704",
            crc::core::Algorithm<std::uint8_t>{
                .width = 5,
                .poly = UINT8_C(0x15),
                .init = UINT8_C(0x00),
                .refin = true,
                .refout = true,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x07),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-5/USB",
            crc::core::Algorithm<std::uint8_t>{
                .width = 5,
                .poly = UINT8_C(0x05),
                .init = UINT8_C(0x1f),
                .refin = true,
                .refout = true,
                .xorout = UINT8_C(0x1f),
                .check = UINT8_C(0x19),
                .residue = UINT8_C(0x06),
            }),

        std::make_tuple(
            "CRC-6/CDMA2000-A",
            crc::core::Algorithm<std::uint8_t>{
                .width = 6,
                .poly = UINT8_C(0x27),
                .init = UINT8_C(0x3f),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x0d),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-6/CDMA2000-B",
            crc::core::Algorithm<std::uint8_t>{
                .width = 6,
                .poly = UINT8_C(0x07),
                .init = UINT8_C(0x3f),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x3b),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-6/DARC",
            crc::core::Algorithm<std::uint8_t>{
                .width = 6,
                .poly = UINT8_C(0x19),
                .init = UINT8_C(0x00),
                .refin = true,
                .refout = true,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x26),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-6/G-704",
            crc::core::Algorithm<std::uint8_t>{
                .width = 6,
                .poly = UINT8_C(0x03),
                .init = UINT8_C(0x00),
                .refin = true,
                .refout = true,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x06),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-6/GSM",
            crc::core::Algorithm<std::uint8_t>{
                .width = 6,
                .poly = UINT8_C(0x2f),
                .init = UINT8_C(0x00),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x3f),
                .check = UINT8_C(0x13),
                .residue = UINT8_C(0x3a),
            }),

        std::make_tuple(
            "CRC-7/MMC",
            crc::core::Algorithm<std::uint8_t>{
                .width = 7,
                .poly = UINT8_C(0x09),
                .init = UINT8_C(0x00),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x75),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-7/ROHC",
            crc::core::Algorithm<std::uint8_t>{
                .width = 7,
                .poly = UINT8_C(0x4f),
                .init = UINT8_C(0x7f),
                .refin = true,
                .refout = true,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x53),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-7/UMTS",
            crc::core::Algorithm<std::uint8_t>{
                .width = 7,
                .poly = UINT8_C(0x45),
                .init = UINT8_C(0x00),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x61),
                .residue = UINT8_C(0x00),
            }),

        std::make_tuple(
            "CRC-8/AUTOSAR",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x2f),
                .init = UINT8_C(0xff),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0xff),
                .check = UINT8_C(0xdf),
                .residue = UINT8_C(0x42),
            }),
        std::make_tuple(
            "CRC-8/BLUETOOTH",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0xa7),
                .init = UINT8_C(0x00),
                .refin = true,
                .refout = true,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x26),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-8/CDMA2000",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x9b),
                .init = UINT8_C(0xff),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0xda),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-8/DARC",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x39),
                .init = UINT8_C(0x00),
                .refin = true,
                .refout = true,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x15),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-8/DVB-S2",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0xd5),
                .init = UINT8_C(0x00),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0xbc),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-8/GSM-A",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x1d),
                .init = UINT8_C(0x00),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x37),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-8/GSM-B",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x49),
                .init = UINT8_C(0x00),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0xff),
                .check = UINT8_C(0x94),
                .residue = UINT8_C(0x53),
            }),
        std::make_tuple(
            "CRC-8/I-432-1",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x07),
                .init = UINT8_C(0x00),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x55),
                .check = UINT8_C(0xa1),
                .residue = UINT8_C(0xac),
            }),
        std::make_tuple(
            "CRC-8/I-CODE",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x1d),
                .init = UINT8_C(0xfd),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x7e),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-8/LTE",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x9b),
                .init = UINT8_C(0x00),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0xea),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-8/MAXIM-DOW",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x31),
                .init = UINT8_C(0x00),
                .refin = true,
                .refout = true,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0xa1),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-8/MIFARE-MAD",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x1d),
                .init = UINT8_C(0xc7),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x99),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-8/NRSC-5",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x31),
                .init = UINT8_C(0xff),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0xf7),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-8/OPENSAFETY",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x2f),
                .init = UINT8_C(0x00),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x3e),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-8/ROHC",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x07),
                .init = UINT8_C(0xff),
                .refin = true,
                .refout = true,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0xd0),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-8/SAE-J1850",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x1d),
                .init = UINT8_C(0xff),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0xff),
                .check = UINT8_C(0x4b),
                .residue = UINT8_C(0xc4),
            }),
        std::make_tuple(
            "CRC-8/SMBUS",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x07),
                .init = UINT8_C(0x00),
                .refin = false,
                .refout = false,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0xf4),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-8/TECH-3250",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x1d),
                .init = UINT8_C(0xff),
                .refin = true,
                .refout = true,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x97),
                .residue = UINT8_C(0x00),
            }),
        std::make_tuple(
            "CRC-8/WCDMA",
            crc::core::Algorithm<std::uint8_t>{
                .width = 8,
                .poly = UINT8_C(0x9b),
                .init = UINT8_C(0x00),
                .refin = true,
                .refout = true,
                .xorout = UINT8_C(0x00),
                .check = UINT8_C(0x25),
                .residue = UINT8_C(0x00),
            }),
    };
    for (const auto &algorithm_pair : crc8_algorithms) {
        test_crc(failures, std::get<0>(algorithm_pair), std::get<1>(algorithm_pair));
    }

    const auto crc16_algorithms = {
        std::make_tuple(
            "CRC-10/ATM",
            crc::core::Algorithm<std::uint16_t>{
                .width = 10,
                .poly = UINT16_C(0x233),
                .init = UINT16_C(0x000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x000),
                .check = UINT16_C(0x199),
                .residue = UINT16_C(0x000),
            }),
        std::make_tuple(
            "CRC-10/CDMA2000",
            crc::core::Algorithm<std::uint16_t>{
                .width = 10,
                .poly = UINT16_C(0x3d9),
                .init = UINT16_C(0x3ff),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x000),
                .check = UINT16_C(0x233),
                .residue = UINT16_C(0x000),
            }),
        std::make_tuple(
            "CRC-10/GSM",
            crc::core::Algorithm<std::uint16_t>{
                .width = 10,
                .poly = UINT16_C(0x175),
                .init = UINT16_C(0x000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x3ff),
                .check = UINT16_C(0x12a),
                .residue = UINT16_C(0x0c6),
            }),

        std::make_tuple(
            "CRC-11/FLEXRAY",
            crc::core::Algorithm<std::uint16_t>{
                .width = 11,
                .poly = UINT16_C(0x385),
                .init = UINT16_C(0x01a),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x000),
                .check = UINT16_C(0x5a3),
                .residue = UINT16_C(0x000),
            }),
        std::make_tuple(
            "CRC-11/UMTS",
            crc::core::Algorithm<std::uint16_t>{
                .width = 11,
                .poly = UINT16_C(0x307),
                .init = UINT16_C(0x000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x000),
                .check = UINT16_C(0x061),
                .residue = UINT16_C(0x000),
            }),

        std::make_tuple(
            "CRC-12/CDMA2000",
            crc::core::Algorithm<std::uint16_t>{
                .width = 12,
                .poly = UINT16_C(0xf13),
                .init = UINT16_C(0xfff),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x000),
                .check = UINT16_C(0xd4d),
                .residue = UINT16_C(0x000),
            }),
        std::make_tuple(
            "CRC-12/DECT",
            crc::core::Algorithm<std::uint16_t>{
                .width = 12,
                .poly = UINT16_C(0x80f),
                .init = UINT16_C(0x000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x000),
                .check = UINT16_C(0xf5b),
                .residue = UINT16_C(0x000),
            }),
        std::make_tuple(
            "CRC-12/GSM",
            crc::core::Algorithm<std::uint16_t>{
                .width = 12,
                .poly = UINT16_C(0xd31),
                .init = UINT16_C(0x000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0xfff),
                .check = UINT16_C(0xb34),
                .residue = UINT16_C(0x178),
            }),
        std::make_tuple(
            "CRC-12/UMTS",
            crc::core::Algorithm<std::uint16_t>{
                .width = 12,
                .poly = UINT16_C(0x80f),
                .init = UINT16_C(0x000),
                .refin = false,
                .refout = true,
                .xorout = UINT16_C(0x000),
                .check = UINT16_C(0xdaf),
                .residue = UINT16_C(0x000),
            }),

        std::make_tuple(
            "CRC-13/BBC",
            crc::core::Algorithm<std::uint16_t>{
                .width = 13,
                .poly = UINT16_C(0x1cf5),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x04fa),
                .residue = UINT16_C(0x0000),
            }),

        std::make_tuple(
            "CRC-14/DARC",
            crc::core::Algorithm<std::uint16_t>{
                .width = 14,
                .poly = UINT16_C(0x0805),
                .init = UINT16_C(0x0000),
                .refin = true,
                .refout = true,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x082d),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-14/GSM",
            crc::core::Algorithm<std::uint16_t>{
                .width = 14,
                .poly = UINT16_C(0x202d),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x3fff),
                .check = UINT16_C(0x30ae),
                .residue = UINT16_C(0x031e),
            }),

        std::make_tuple(
            "CRC-15/CAN",
            crc::core::Algorithm<std::uint16_t>{
                .width = 15,
                .poly = UINT16_C(0x4599),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x059e),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-15/MPT1327",
            crc::core::Algorithm<std::uint16_t>{
                .width = 15,
                .poly = UINT16_C(0x6815),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0001),
                .check = UINT16_C(0x2566),
                .residue = UINT16_C(0x6815),
            }),

        std::make_tuple(
            "CRC-16/ARC",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x8005),
                .init = UINT16_C(0x0000),
                .refin = true,
                .refout = true,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0xbb3d),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/CDMA2000",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0xc867),
                .init = UINT16_C(0xffff),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x4c06),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/CMS",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x8005),
                .init = UINT16_C(0xffff),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0xaee7),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/DDS-110",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x8005),
                .init = UINT16_C(0x800d),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x9ecf),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/DECT-R",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x0589),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0001),
                .check = UINT16_C(0x007e),
                .residue = UINT16_C(0x0589),
            }),
        std::make_tuple(
            "CRC-16/DECT-X",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x0589),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x007f),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/DNP",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x3d65),
                .init = UINT16_C(0x0000),
                .refin = true,
                .refout = true,
                .xorout = UINT16_C(0xffff),
                .check = UINT16_C(0xea82),
                .residue = UINT16_C(0x66c5),
            }),
        std::make_tuple(
            "CRC-16/EN-13757",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x3d65),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0xffff),
                .check = UINT16_C(0xc2b7),
                .residue = UINT16_C(0xa366),
            }),
        std::make_tuple(
            "CRC-16/GENIBUS",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x1021),
                .init = UINT16_C(0xffff),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0xffff),
                .check = UINT16_C(0xd64e),
                .residue = UINT16_C(0x1d0f),
            }),
        std::make_tuple(
            "CRC-16/GSM",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x1021),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0xffff),
                .check = UINT16_C(0xce3c),
                .residue = UINT16_C(0x1d0f),
            }),
        std::make_tuple(
            "CRC-16/IBM-3740",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x1021),
                .init = UINT16_C(0xffff),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x29b1),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/IBM-SDLC",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x1021),
                .init = UINT16_C(0xffff),
                .refin = true,
                .refout = true,
                .xorout = UINT16_C(0xffff),
                .check = UINT16_C(0x906e),
                .residue = UINT16_C(0xf0b8),
            }),
        std::make_tuple(
            "CRC-16/ISO-IEC-14443-3-A",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x1021),
                .init = UINT16_C(0xc6c6),
                .refin = true,
                .refout = true,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0xbf05),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/KERMIT",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x1021),
                .init = UINT16_C(0x0000),
                .refin = true,
                .refout = true,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x2189),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/LJ1200",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x6f63),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0xbdf4),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/MAXIM-DOW",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x8005),
                .init = UINT16_C(0x0000),
                .refin = true,
                .refout = true,
                .xorout = UINT16_C(0xffff),
                .check = UINT16_C(0x44c2),
                .residue = UINT16_C(0xb001),
            }),
        std::make_tuple(
            "CRC-16/MCRF4XX",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x1021),
                .init = UINT16_C(0xffff),
                .refin = true,
                .refout = true,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x6f91),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/MODBUS",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x8005),
                .init = UINT16_C(0xffff),
                .refin = true,
                .refout = true,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x4b37),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/NRSC-5",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x080b),
                .init = UINT16_C(0xffff),
                .refin = true,
                .refout = true,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0xa066),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/OPENSAFETY-A",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x5935),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x5d38),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/OPENSAFETY-B",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x755b),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x20fe),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/PROFIBUS",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x1dcf),
                .init = UINT16_C(0xffff),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0xffff),
                .check = UINT16_C(0xa819),
                .residue = UINT16_C(0xe394),
            }),
        std::make_tuple(
            "CRC-16/RIELLO",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x1021),
                .init = UINT16_C(0xb2aa),
                .refin = true,
                .refout = true,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x63d0),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/SPI-FUJITSU",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x1021),
                .init = UINT16_C(0x1d0f),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0xe5cc),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/T10-DIF",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x8bb7),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0xd0db),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/TELEDISK",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0xa097),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x0fb3),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/TMS37157",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x1021),
                .init = UINT16_C(0x89ec),
                .refin = true,
                .refout = true,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x26b1),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/UMTS",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x8005),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0xfee8),
                .residue = UINT16_C(0x0000),
            }),
        std::make_tuple(
            "CRC-16/USB",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x8005),
                .init = UINT16_C(0xffff),
                .refin = true,
                .refout = true,
                .xorout = UINT16_C(0xffff),
                .check = UINT16_C(0xb4c8),
                .residue = UINT16_C(0xb001),
            }),
        std::make_tuple(
            "CRC-16/XMODEM",
            crc::core::Algorithm<std::uint16_t>{
                .width = 16,
                .poly = UINT16_C(0x1021),
                .init = UINT16_C(0x0000),
                .refin = false,
                .refout = false,
                .xorout = UINT16_C(0x0000),
                .check = UINT16_C(0x31c3),
                .residue = UINT16_C(0x0000),
            }),
    };
    for (const auto &algorithm_pair : crc16_algorithms) {
        test_crc(failures, std::get<0>(algorithm_pair), std::get<1>(algorithm_pair));
    }

    const auto crc32_algorithms = {
        std::make_tuple(
            "CRC-17/CAN-FD",
            crc::core::Algorithm<std::uint32_t>{
                .width = 17,
                .poly = UINT32_C(0x1685b),
                .init = UINT32_C(0x00000),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0x00000),
                .check = UINT32_C(0x04f03),
                .residue = UINT32_C(0x00000),
            }),

        std::make_tuple(
            "CRC-21/CAN-FD",
            crc::core::Algorithm<std::uint32_t>{
                .width = 21,
                .poly = UINT32_C(0x102899),
                .init = UINT32_C(0x000000),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0x000000),
                .check = UINT32_C(0x0ed841),
                .residue = UINT32_C(0x000000),
            }),

        std::make_tuple(
            "CRC-24/BLE",
            crc::core::Algorithm<std::uint32_t>{
                .width = 24,
                .poly = UINT32_C(0x00065b),
                .init = UINT32_C(0x555555),
                .refin = true,
                .refout = true,
                .xorout = UINT32_C(0x000000),
                .check = UINT32_C(0xc25a56),
                .residue = UINT32_C(0x000000),
            }),
        std::make_tuple(
            "CRC-24/FLEXRAY-A",
            crc::core::Algorithm<std::uint32_t>{
                .width = 24,
                .poly = UINT32_C(0x5d6dcb),
                .init = UINT32_C(0xfedcba),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0x000000),
                .check = UINT32_C(0x7979bd),
                .residue = UINT32_C(0x000000),
            }),
        std::make_tuple(
            "CRC-24/FLEXRAY-B",
            crc::core::Algorithm<std::uint32_t>{
                .width = 24,
                .poly = UINT32_C(0x5d6dcb),
                .init = UINT32_C(0xabcdef),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0x000000),
                .check = UINT32_C(0x1f23b8),
                .residue = UINT32_C(0x000000),
            }),
        std::make_tuple(
            "CRC-24/INTERLAKEN",
            crc::core::Algorithm<std::uint32_t>{
                .width = 24,
                .poly = UINT32_C(0x328b63),
                .init = UINT32_C(0xffffff),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0xffffff),
                .check = UINT32_C(0xb4f3e6),
                .residue = UINT32_C(0x144e63),
            }),
        std::make_tuple(
            "CRC-24/LTE-A",
            crc::core::Algorithm<std::uint32_t>{
                .width = 24,
                .poly = UINT32_C(0x864cfb),
                .init = UINT32_C(0x000000),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0x000000),
                .check = UINT32_C(0xcde703),
                .residue = UINT32_C(0x000000),
            }),
        std::make_tuple(
            "CRC-24/OPENPGP",
            crc::core::Algorithm<std::uint32_t>{
                .width = 24,
                .poly = UINT32_C(0x864cfb),
                .init = UINT32_C(0xb704ce),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0x000000),
                .check = UINT32_C(0x21cf02),
                .residue = UINT32_C(0x000000),
            }),
        std::make_tuple(
            "CRC-24/OS-9",
            crc::core::Algorithm<std::uint32_t>{
                .width = 24,
                .poly = UINT32_C(0x800063),
                .init = UINT32_C(0xffffff),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0xffffff),
                .check = UINT32_C(0x200fa5),
                .residue = UINT32_C(0x800fe3),
            }),

        std::make_tuple(
            "CRC-30/CDMA",
            crc::core::Algorithm<std::uint32_t>{
                .width = 30,
                .poly = UINT32_C(0x2030b9c7),
                .init = UINT32_C(0x3fffffff),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0x3fffffff),
                .check = UINT32_C(0x04c34abf),
                .residue = UINT32_C(0x34efa55a),
            }),

        std::make_tuple(
            "CRC-31/PHILIPS",
            crc::core::Algorithm<std::uint32_t>{
                .width = 31,
                .poly = UINT32_C(0x04c11db7),
                .init = UINT32_C(0x7fffffff),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0x7fffffff),
                .check = UINT32_C(0x0ce9e46c),
                .residue = UINT32_C(0x4eaf26f1),
            }),

        std::make_tuple(
            "CRC-32/AIXM",
            crc::core::Algorithm<std::uint32_t>{
                .width = 32,
                .poly = UINT32_C(0x814141ab),
                .init = UINT32_C(0x00000000),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0x00000000),
                .check = UINT32_C(0x3010bf7f),
                .residue = UINT32_C(0x00000000),
            }),
        std::make_tuple(
            "CRC-32/AUTOSAR",
            crc::core::Algorithm<std::uint32_t>{
                .width = 32,
                .poly = UINT32_C(0xf4acfb13),
                .init = UINT32_C(0xffffffff),
                .refin = true,
                .refout = true,
                .xorout = UINT32_C(0xffffffff),
                .check = UINT32_C(0x1697d06a),
                .residue = UINT32_C(0x904cddbf),
            }),
        std::make_tuple(
            "CRC-32/BASE91-D",
            crc::core::Algorithm<std::uint32_t>{
                .width = 32,
                .poly = UINT32_C(0xa833982b),
                .init = UINT32_C(0xffffffff),
                .refin = true,
                .refout = true,
                .xorout = UINT32_C(0xffffffff),
                .check = UINT32_C(0x87315576),
                .residue = UINT32_C(0x45270551),
            }),
        std::make_tuple(
            "CRC-32/BZIP2",
            crc::core::Algorithm<std::uint32_t>{
                .width = 32,
                .poly = UINT32_C(0x04c11db7),
                .init = UINT32_C(0xffffffff),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0xffffffff),
                .check = UINT32_C(0xfc891918),
                .residue = UINT32_C(0xc704dd7b),
            }),
        std::make_tuple(
            "CRC-32/CD-ROM-EDC",
            crc::core::Algorithm<std::uint32_t>{
                .width = 32,
                .poly = UINT32_C(0x8001801b),
                .init = UINT32_C(0x00000000),
                .refin = true,
                .refout = true,
                .xorout = UINT32_C(0x00000000),
                .check = UINT32_C(0x6ec2edc4),
                .residue = UINT32_C(0x00000000),
            }),
        std::make_tuple(
            "CRC-32/CKSUM",
            crc::core::Algorithm<std::uint32_t>{
                .width = 32,
                .poly = UINT32_C(0x04c11db7),
                .init = UINT32_C(0x00000000),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0xffffffff),
                .check = UINT32_C(0x765e7680),
                .residue = UINT32_C(0xc704dd7b),
            }),
        std::make_tuple(
            "CRC-32/ISCSI",
            crc::core::Algorithm<std::uint32_t>{
                .width = 32,
                .poly = UINT32_C(0x1edc6f41),
                .init = UINT32_C(0xffffffff),
                .refin = true,
                .refout = true,
                .xorout = UINT32_C(0xffffffff),
                .check = UINT32_C(0xe3069283),
                .residue = UINT32_C(0xb798b438),
            }),
        std::make_tuple(
            "CRC-32/ISO-HDLC",
            crc::core::Algorithm<std::uint32_t>{
                .width = 32,
                .poly = UINT32_C(0x04c11db7),
                .init = UINT32_C(0xffffffff),
                .refin = true,
                .refout = true,
                .xorout = UINT32_C(0xffffffff),
                .check = UINT32_C(0xcbf43926),
                .residue = UINT32_C(0xdebb20e3),
            }),
        std::make_tuple(
            "CRC-32/JAMCRC",
            crc::core::Algorithm<std::uint32_t>{
                .width = 32,
                .poly = UINT32_C(0x04c11db7),
                .init = UINT32_C(0xffffffff),
                .refin = true,
                .refout = true,
                .xorout = UINT32_C(0x00000000),
                .check = UINT32_C(0x340bc6d9),
                .residue = UINT32_C(0x00000000),
            }),
        std::make_tuple(
            "CRC-32/MPEG-2",
            crc::core::Algorithm<std::uint32_t>{
                .width = 32,
                .poly = UINT32_C(0x04c11db7),
                .init = UINT32_C(0xffffffff),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0x00000000),
                .check = UINT32_C(0x0376e6e7),
                .residue = UINT32_C(0x00000000),
            }),
        std::make_tuple(
            "CRC-32/XFER",
            crc::core::Algorithm<std::uint32_t>{
                .width = 32,
                .poly = UINT32_C(0x000000af),
                .init = UINT32_C(0x00000000),
                .refin = false,
                .refout = false,
                .xorout = UINT32_C(0x00000000),
                .check = UINT32_C(0xbd0be338),
                .residue = UINT32_C(0x00000000),
            }),
    };
    for (const auto &algorithm_pair : crc32_algorithms) {
        test_crc(failures, std::get<0>(algorithm_pair), std::get<1>(algorithm_pair));
    }

    const auto crc64_algorithms = {
        std::make_tuple(
            "CRC-40/GSM",
            crc::core::Algorithm<std::uint64_t>{
                .width = 40,
                .poly = UINT64_C(0x0004820009),
                .init = UINT64_C(0x0000000000),
                .refin = false,
                .refout = false,
                .xorout = UINT64_C(0xffffffffff),
                .check = UINT64_C(0xd4164fc646),
                .residue = UINT64_C(0xc4ff8071ff),
            }),

        std::make_tuple(
            "CRC-64/ECMA-182",
            crc::core::Algorithm<std::uint64_t>{
                .width = 64,
                .poly = UINT64_C(0x42f0e1eba9ea3693),
                .init = UINT64_C(0x0000000000000000),
                .refin = false,
                .refout = false,
                .xorout = UINT64_C(0x0000000000000000),
                .check = UINT64_C(0x6c40df5f0b497347),
                .residue = UINT64_C(0x0000000000000000),
            }),
        std::make_tuple(
            "CRC-64/GO-ISO",
            crc::core::Algorithm<std::uint64_t>{
                .width = 64,
                .poly = UINT64_C(0x000000000000001b),
                .init = UINT64_C(0xffffffffffffffff),
                .refin = true,
                .refout = true,
                .xorout = UINT64_C(0xffffffffffffffff),
                .check = UINT64_C(0xb90956c775a41001),
                .residue = UINT64_C(0x5300000000000000),
            }),
        std::make_tuple(
            "CRC-64/WE",
            crc::core::Algorithm<std::uint64_t>{
                .width = 64,
                .poly = UINT64_C(0x42f0e1eba9ea3693),
                .init = UINT64_C(0xffffffffffffffff),
                .refin = false,
                .refout = false,
                .xorout = UINT64_C(0xffffffffffffffff),
                .check = UINT64_C(0x62ec59e3f1a4f00a),
                .residue = UINT64_C(0xfcacbebd5931a992),
            }),
        std::make_tuple(
            "CRC-64/XZ",
            crc::core::Algorithm<std::uint64_t>{
                .width = 64,
                .poly = UINT64_C(0x42f0e1eba9ea3693),
                .init = UINT64_C(0xffffffffffffffff),
                .refin = true,
                .refout = true,
                .xorout = UINT64_C(0xffffffffffffffff),
                .check = UINT64_C(0x995dc9bbdf1939fa),
                .residue = UINT64_C(0x49958c9abd7d353f),
            }),
    };
    for (const auto &algorithm_pair : crc64_algorithms) {
        test_crc(failures, std::get<0>(algorithm_pair), std::get<1>(algorithm_pair));
    }

#if 0
    const auto crc128_algorithms = {
        std::make_tuple(
            "CRC-82/DARC",
            crc::core::Algorithm<std::uint128_t>{
                .width = 82,
                .poly = UINT128_C(0x0308c0111011401440411),
                .init = UINT128_C(0x000000000000000000000),
                .refin = true,
                .refout = true,
                .xorout = UINT128_C(0x000000000000000000000),
                .check = UINT128_C(0x09ea83f625023801fd612),
                .residue = UINT128_C(0x000000000000000000000),
            }),
    };
    for (const auto &algorithm_pair : crc128_algorithms) {
        test_crc(failures, std::get<0>(algorithm_pair), std::get<1>(algorithm_pair));
    }
#endif

    if (failures.empty()) {
        return 0;
    }

    std::cerr << std::hex << std::showbase;
    for (const auto &[name, calculated_crc, expected_crc] : failures) {
        std::cerr << name << ": " << calculated_crc << " != " << expected_crc << std::endl;
    }
    return 1;
}
