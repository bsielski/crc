#include "crc.h"

namespace crc {

static constexpr core::Algorithm<std::uint32_t> castagnoli_algorithm = {
    .width = 32,
    .poly = UINT32_C(0x1edc6f41),
    .init = UINT32_C(0xffffffff),
    .refin = true,
    .refout = true,
    .xorout = UINT32_C(0xffffffff),
    .check = UINT32_C(0xe3069283),
    .residue = UINT32_C(0xb798b438),
};

const core::Crc<std::uint32_t> castagnoli(castagnoli_algorithm);

} // namespace crc

extern "C" {

uint32_t crc_castagnoli_checksum(const uint8_t *data, size_t data_size)
{
    return crc::castagnoli.checksum(data, data_size);
}

uint32_t crc_castagnoli_init()
{
    return crc::castagnoli.init();
}

void crc_castagnoli_update(uint32_t *crc, const uint8_t *data, size_t data_size)
{
    return crc::castagnoli.update(*crc, data, data_size);
}

void crc_castagnoli_finalize(uint32_t *crc)
{
    return crc::castagnoli.finalize(*crc);
}

} // extern "C"
