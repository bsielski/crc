#pragma once
#include <crc_core.h>
#include <stddef.h>

#ifdef __cplusplus

namespace crc {

extern const core::Crc<std::uint32_t> castagnoli;

}

extern "C" {
#endif // __cplusplus

uint32_t crc_castagnoli_checksum(const uint8_t *data, size_t data_size);
uint32_t crc_castagnoli_init();
void crc_castagnoli_update(uint32_t *crc, const uint8_t *data, size_t data_size);
void crc_castagnoli_finalize(uint32_t *crc);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus
