CRC core
========

A header-only, fully constexpr CRC library designed to make it easy to create
your own library containing only the CRC algorithms that you use.

Usage:
------

.. code:: c++

  #include <crc_core.h>

  // Define the CRC algorithm you want to use
  const crc::core::Algorithm<std::uint32_t> crc_castagnoli_algorithm = {
      .width = 32,
      .poly = UINT32_C(0x1edc6f41),
      .init = UINT32_C(0xffffffff),
      .refin = true,
      .refout = true,
      .xorout = UINT32_C(0xffffffff),
      .check = UINT32_C(0xe3069283),
      .residue = UINT32_C(0xb798b438),
  };
  const crc::core::Crc<std::uint32_t> crc_castagnoli(crc_castagnoli_algorithm);

  // Calculate the checksum of a block of data
  std::uint32_t calculated_crc = crc_castagnoli.checksum(input_data, input_data_size);

  // Alternatively, if data is split up in chunks, the calculation of the
  // checksum can be done piece by piece as well
  std::uint32_t calculated_crc = crc_castagnoli.init();
  crc_castagnoli.update(calculated_crc, input_data_1, input_data_size_1);
  crc_castagnoli.update(calculated_crc, input_data_2, input_data_size_2);
  crc_castagnoli.finalize(calculated_crc);

This way of using the library is fine, but for larger projects, where the CRCs
need to be calculated in multiple places (possibly using different algorithms),
it is recommended to create a library which only exposes the needed
``crc::core::Crc`` objects.  An example of such a library can be found in
``examples/lib``.
