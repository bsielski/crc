#pragma once
#include <array>
#include <cstdint>

#ifdef __cplusplus

namespace crc::core {

namespace {

template<typename T>
constexpr std::uint8_t bit_width() noexcept
{
    return sizeof(T) * 8;
}

constexpr std::uint8_t bit_reverse(std::uint8_t x) noexcept
{
#ifdef __clang__
    return __builtin_bitreverse8(x);
#else
    x = (x & UINT8_C(0xF0)) >> 4 | (x & UINT8_C(0x0F)) << 4;
    x = (x & UINT8_C(0xCC)) >> 2 | (x & UINT8_C(0x33)) << 2;
    x = (x & UINT8_C(0xAA)) >> 1 | (x & UINT8_C(0x55)) << 1;
    return x;
#endif
}

constexpr std::uint16_t bit_reverse(std::uint16_t x) noexcept
{
#ifdef __clang__
    return __builtin_bitreverse16(x);
#else
#ifdef __GNUC__
    x = __builtin_bswap16(x);
#else
    x = (x & UINT16_C(0xFF00)) >> 8 | (x & UINT16_C(0x00FF)) << 8;
#endif
    x = (x & UINT16_C(0xF0F0)) >> 4 | (x & UINT16_C(0x0F0F)) << 4;
    x = (x & UINT16_C(0xCCCC)) >> 2 | (x & UINT16_C(0x3333)) << 2;
    x = (x & UINT16_C(0xAAAA)) >> 1 | (x & UINT16_C(0x5555)) << 1;
    return x;
#endif
}

constexpr std::uint32_t bit_reverse(std::uint32_t x) noexcept
{
#ifdef __clang__
    return __builtin_bitreverse32(x);
#else
#ifdef __GNUC__
    x = __builtin_bswap32(x);
#else
    x = (x & UINT32_C(0xFFFF0000)) >> 16 | (x & UINT32_C(0x0000FFFF)) << 16;
    x = (x & UINT32_C(0xFF00FF00)) >> 8 | (x & UINT32_C(0x00FF00FF)) << 8;
#endif
    x = (x & UINT32_C(0xF0F0F0F0)) >> 4 | (x & UINT32_C(0x0F0F0F0F)) << 4;
    x = (x & UINT32_C(0xCCCCCCCC)) >> 2 | (x & UINT32_C(0x33333333)) << 2;
    x = (x & UINT32_C(0xAAAAAAAA)) >> 1 | (x & UINT32_C(0x55555555)) << 1;
    return x;
#endif
}

constexpr std::uint64_t bit_reverse(std::uint64_t x) noexcept
{
#ifdef __clang__
    return __builtin_bitreverse64(x);
#else
#ifdef __GNUC__
    x = __builtin_bswap64(x);
#else
    x = (x & UINT64_C(0xFFFFFFFF00000000)) >> 32 | (x & UINT64_C(0x00000000FFFFFFFF)) << 32;
    x = (x & UINT64_C(0xFFFF0000FFFF0000)) >> 16 | (x & UINT64_C(0x0000FFFF0000FFFF)) << 16;
    x = (x & UINT64_C(0xFF00FF00FF00FF00)) >> 8 | (x & UINT64_C(0x00FF00FF00FF00FF)) << 8;
#endif
    x = (x & UINT64_C(0xF0F0F0F0F0F0F0F0)) >> 4 | (x & UINT64_C(0x0F0F0F0F0F0F0F0F)) << 4;
    x = (x & UINT64_C(0xCCCCCCCCCCCCCCCC)) >> 2 | (x & UINT64_C(0x3333333333333333)) << 2;
    x = (x & UINT64_C(0xAAAAAAAAAAAAAAAA)) >> 1 | (x & UINT64_C(0x5555555555555555)) << 1;
    return x;
#endif
}

} // namespace

template<typename T>
struct Algorithm {
    std::uint8_t width;
    T poly;
    T init;
    bool refin;
    bool refout;
    T xorout;
    T check;
    T residue;

    constexpr std::uint8_t offset() const noexcept
    {
        return bit_width<T>() - this->width;
    }
};

template<typename T>
constexpr std::array<T, 256> gen_table(const Algorithm<T> &algorithm) noexcept
{
    T poly = algorithm.poly;
    if (algorithm.refin) {
        poly = bit_reverse(poly);
        poly >>= algorithm.offset();
    } else {
        poly <<= algorithm.offset();
    }

    std::array<T, 256> table;
    if (algorithm.refin) {
        for (std::size_t i = 0; i < table.size(); ++i) {
            T value = static_cast<T>(i);
            for (int j = 0; j < 8; ++j) {
                value = (value >> 1) ^ ((value & 1) * poly);
            }
            table[i] = value;
        }
    } else {
        for (std::size_t i = 0; i < table.size(); ++i) {
            T value = static_cast<T>(i) << (bit_width<T>() - 8);
            for (int j = 0; j < 8; ++j) {
                value = (value << 1) ^ (((value >> (bit_width<T>() - 1)) & 1) * poly);
            }
            table[i] = value;
        }
    }
    return table;
}

template<typename T>
struct Crc {
    const Algorithm<T> &algorithm;
    const std::array<T, 256> table;

    constexpr Crc(const Algorithm<T> &algorithm) noexcept
        : algorithm(algorithm), table(gen_table(algorithm))
    {
    }

    constexpr T checksum(const std::uint8_t *buf, std::size_t buf_len) const noexcept
    {
        T crc = this->init();
        this->update(crc, buf, buf_len);
        this->finalize(crc);
        return crc;
    }

    constexpr T init() const noexcept
    {
        T init = this->algorithm.init;
        if (this->algorithm.refin) {
            init = bit_reverse(init);
            init >>= this->algorithm.offset();
        } else {
            init <<= this->algorithm.offset();
        }
        return init;
    }

    constexpr void update(T &crc, const std::uint8_t *buf, std::size_t buf_len) const noexcept
    {
        if (this->algorithm.refin) {
            for (std::size_t i = 0; i < buf_len; ++i) {
                std::uint8_t table_index = crc ^ buf[i];
                crc = this->table[table_index] ^ (crc >> 8);
            }
        } else {
            for (std::size_t i = 0; i < buf_len; ++i) {
                std::uint8_t table_index = (crc >> (bit_width<T>() - 8)) ^ buf[i];
                crc = this->table[table_index] ^ (crc << 8);
            }
        }
    }

    constexpr void finalize(T &crc) const noexcept
    {
        if (this->algorithm.refin != this->algorithm.refout) {
            crc = bit_reverse(crc);
        }
        if (!this->algorithm.refout) {
            crc >>= this->algorithm.offset();
        }
        crc ^= this->algorithm.xorout;
    }
};

template<>
constexpr void Crc<std::uint8_t>::update(
    std::uint8_t &crc, const std::uint8_t *buf, std::size_t buf_len) const noexcept
{
    for (std::size_t i = 0; i < buf_len; ++i) {
        crc = this->table[crc ^ buf[i]];
    }
}

} // namespace crc::core

#endif // __cplusplus
